package handler

import (
	"net/http"

	"github.com/Jeanpigi/scraping/citas"
	"github.com/gin-gonic/gin"
)

type QuotePostRequest struct {
	Quote  string `json:"Quote"`
	Author string `json:"Author"`
}

func QuotePost(cita citas.Adder) gin.HandlerFunc {
	return func(c *gin.Context) {
		requestBody := QuotePostRequest{}
		c.Bind(&requestBody)

		quote := citas.Quote{
			Quote:  requestBody.Quote,
			Author: requestBody.Author,
		}

		cita.Add(quote)

		c.Status(http.StatusNoContent)
	}

}
