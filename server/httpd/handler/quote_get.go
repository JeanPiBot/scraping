package handler

import (
	"net/http"

	"github.com/Jeanpigi/scraping/citas"
	"github.com/gin-gonic/gin"
)

func QuoteGet(cita citas.Getter) gin.HandlerFunc {
	return func(c *gin.Context) {
		results := cita.GetAll()
		c.JSON(http.StatusOK, results)
	}
}
