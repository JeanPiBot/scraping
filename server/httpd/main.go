package main

import (
	"fmt"
	"log"

	"github.com/Jeanpigi/scraping/citas"
	"github.com/Jeanpigi/scraping/httpd/handler"
	"github.com/gin-gonic/gin"
	"github.com/gocolly/colly"
)

func main() {
	cita := citas.New()

	c := colly.NewCollector()

	c.OnError(func(_ *colly.Response, err error) {
		log.Println("Something went wrong:", err)
	})

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL.String())
	})

	c.OnHTML("div.quote", func(e *colly.HTMLElement) {
		quote := e.ChildText("span.text")
		author := e.ChildText("small.author")

		cita.Add(citas.Quote{
			Quote:  quote,
			Author: author,
		})

	})

	c.OnHTML(`.next a[href]`, func(e *colly.HTMLElement) {
		link := e.Attr("href")

		//In case do not find a link
		if link == "" {
			log.Println("No link found", e.Request.URL)
		}
		// Print link
		fmt.Printf("Link found: %s\n", link)
		e.Request.Visit(link)
	})

	c.OnResponse(func(r *colly.Response) {
		fmt.Println("Visited", r.Request.URL)
	})

	c.Visit("http://quotes.toscrape.com/")

	r := gin.Default()

	r.Static("/js", "../../../client/dist/bundle.js")

	r.LoadHTMLFiles("../../../client/dist/index.html")

	apiRoutes := r.Group("/api")
	{
		apiRoutes.GET("/ping", handler.PingGet())
		apiRoutes.GET("/quotes", handler.QuoteGet(cita))
		apiRoutes.POST("/quotes", handler.QuotePost(cita))
	}

	viewRoutes := r.Group("/client/dist")
	{
		viewRoutes.GET("/")
	}

	r.Run(":8000")

}
